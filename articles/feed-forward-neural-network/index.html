<div id="preamble">
<div class="sectionbody">
<div class="paragraph">
<p>The best way to learn about ideas is to learn how those ideas emerged in the first place. Where do neural networks come from ? To answer this question, out journey will start by defining a simple linear model. From there on, by refining the idea and adding complexity to it, we will get what we call a feed-forward neural network. Understanding how it works with all the math behind their functioning will enable us to understand more complex types of networks later.</p>
</div>
<div class="paragraph">
<p>I will show the mathematical details of our operations in a way that I hope is understandable to people who stopped doing math in high school. After reading it, you will have a grasp of matrices operations, how and why it applies to neural networks.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_building_a_simple_neural_network">Building a simple neural network</h2>
<div class="sectionbody">
<div class="paragraph">
<p>Let&#8217;s say we want to build a model that predicts people&#8217;s <strong>salary</strong> given only their <strong>age</strong> and <strong>height</strong>.</p>
</div>
<div class="paragraph">
<p>We want this model to be extra simple, so we&#8217;ll restrict the possibilities to a linear combination of the inputs, namely <strong>age</strong> and <strong>height</strong>. You probably remember that the formula of a line is <span class="image"><img src="feed-forward-neural-network/stem-2b3b2222209509dce25080749584e6d8.png" alt="stem 2b3b2222209509dce25080749584e6d8" width="131" height="18"></span> ? Our model&#8217;s formula will look like this :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-9db7bbb9a226b2c6b67960782474e580.png" alt="$$salary(age, height) = coefficient_{age} \cdot age + coefficient_{height} \cdot height + bias$$" width="695" height="21">
</div>
</div>
<div class="paragraph">
<p>The <span class="image"><img src="feed-forward-neural-network/stem-912f7a9171c37be275b43cd962567017.png" alt="stem 912f7a9171c37be275b43cd962567017" width="38" height="14"></span> term has the same function as the <span class="image"><img src="feed-forward-neural-network/stem-4bdc8d9bcfb35e1c9bfb51fc69687dfc.png" alt="stem 4bdc8d9bcfb35e1c9bfb51fc69687dfc" width="11" height="14"></span> term present in the equation of a line.</p>
</div>
<div class="paragraph">
<p>Our whole model will be determined by, and only by the following parameters :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-efbe076c2b0f2b81d3dd7998a1b9099f.png" alt="$$(coefficient_{age}, coefficient_{height}, bias)$$" width="363" height="21">
</div>
</div>
<div class="paragraph">
<p>We say that our model has <strong>three parameters</strong>. Those will respectively represent :</p>
</div>
<div class="ulist">
<ul>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-6c890b30e3d85eb308c54dfed31ae941.png" alt="stem 6c890b30e3d85eb308c54dfed31ae941" width="135" height="18"></span> : How the <strong>age</strong> contributes to the <strong>salary</strong></p>
</li>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-9a9b1392070ac44be985e4ae266bf74c.png" alt="stem 9a9b1392070ac44be985e4ae266bf74c" width="153" height="20"></span> : How the <strong>height</strong> contributes to the <strong>salary</strong></p>
</li>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-912f7a9171c37be275b43cd962567017.png" alt="stem 912f7a9171c37be275b43cd962567017" width="38" height="14"></span> : How all of this is adjusted. Think of what have to be the result of <span class="image"><img src="feed-forward-neural-network/stem-53c78bbbee9a014f39bdb278be24c0b9.png" alt="stem 53c78bbbee9a014f39bdb278be24c0b9" width="107" height="18"></span> for our model to be a good approximation of reality</p>
</li>
</ul>
</div>
<div class="paragraph">
<p>Here is a graphic representing what such model looks like. The parameters here are <span class="image"><img src="feed-forward-neural-network/stem-317950537f515b0004bd859f00bd38c7.png" alt="stem 317950537f515b0004bd859f00bd38c7" width="111" height="18"></span> :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/sigmoid.png" alt="sigmoid" width="640" height="480">
</div>
<div class="title">Figure 1. A visual example for salary = age * 1.5 + height * 1.1 + 50</div>
</div>
<div class="paragraph">
<p>At this point you might wonder.</p>
</div>
<div class="paragraph">
<p>Ok sure that&#8217;s great but how does this relate to a neural network ? Those are supposed to be difficult and this looks way too easy.</p>
</div>
<div class="paragraph">
<p>In fact what we defined can already be modelized by a very simple neural network consisting of two input nodes, <strong>age</strong> and <strong>height</strong>, and one output node, the predicted <strong>salary</strong>. Here&#8217;s a visual representation of it :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/simple_ffnn.png" alt="Simple Feed-Forward Neural Network" width="415" height="331">
</div>
<div class="title">Figure 2. A very simple Feed-Forward Neural Network</div>
</div>
<div class="ulist">
<ul>
<li>
<p><strong>The input layer is what you feed into the neural network</strong>, it could be an image, a book, a song, data from a company&#8217;s sales department, etc.</p>
</li>
<li>
<p><strong>The output layer is what the neural network predicts</strong>. It could be a sum of money, the probabilities that an image contains a car or a tree, the probability that a piece of music is rock or pop music, etc.</p>
</li>
<li>
<p><strong>The bias is modelized like any other parameter but with a value of <span class="image"><img src="feed-forward-neural-network/stem-034d0a6be0424bffe9a6e7ac9236c0f5.png" alt="stem 034d0a6be0424bffe9a6e7ac9236c0f5" width="8" height="12"></span>.</strong></p>
</li>
<li>
<p><strong>The parameters are called weights.</strong></p>
</li>
</ul>
</div>
<div class="paragraph">
<p>In the image above, the things that are actually happening are hidden, let&#8217;s see it in details :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/ffnn-unit.png" alt="ffnn unit">
</div>
<div class="title">Figure 3. The inside of a Feed-Forward Neural Network</div>
</div>
<div class="paragraph">
<p>We will discuss about the <span class="image"><img src="feed-forward-neural-network/stem-c2f42e565977da428162d2fcff949621.png" alt="stem c2f42e565977da428162d2fcff949621" width="95" height="14"></span> function in a later section. For now we will take <span class="image"><img src="feed-forward-neural-network/stem-75bc06fd7c00a77b4071eedee1544865.png" alt="stem 75bc06fd7c00a77b4071eedee1544865" width="153" height="18"></span>, which means we can just ignore it. So what we get as output is :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-3e0b3f5d04a04c321031078e05b2ac0d.png" alt="$$out_1 = salary(age, height)$$" width="239" height="18">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-979167974c3f1e997865c952e1bd49f2.png" alt="$$out_1 = activation(\sum_{i = 0}^{n\_in}(in_i \cdot weight_i))$$" width="318" height="56">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-69eb98a7aa4074d2db7f0f3fbebec95c.png" alt="$$out_1 = \sum_{i = 0}^{n\_in}(in_i \cdot weight_i)$$" width="210" height="56">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-641224422913c5d0a105470250e1a1fe.png" alt="$$out_1 = 1 \cdot bias + age \cdot coefficient_{age} + height \cdot coefficient_{height}$$" width="576" height="20">
</div>
</div>
<div class="paragraph">
<p>Remember the formula we started with ?</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-9db7bbb9a226b2c6b67960782474e580.png" alt="$$salary(age, height) = coefficient_{age} \cdot age + coefficient_{height} \cdot height + bias$$" width="695" height="21">
</div>
</div>
<div class="paragraph">
<p>It&#8217;s exactly the same ! The important things you should remember here is that :</p>
</div>
<div class="admonitionblock important">
<table>
<tr>
<td class="icon">
<div class="title">Important</div>
</td>
<td class="content">
The weights are the parameters of the network.
</td>
</tr>
</table>
</div>
<div class="paragraph">
<p>And :</p>
</div>
<div class="admonitionblock important">
<table>
<tr>
<td class="icon">
<div class="title">Important</div>
</td>
<td class="content">
The output of the model is called the prediction.
</td>
</tr>
</table>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_a_slighly_more_complex_example">A slighly more complex example</h2>
<div class="sectionbody">
<div class="paragraph">
<p>This is the neural network we will focus on in this section :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/ffnn.png" alt="A Feed-Forward Neural Network with two hidden layer" width="1009" height="648">
</div>
<div class="title">Figure 4. A Feed-Forward Neural Network with two hidden layer</div>
</div>
<div class="paragraph">
<p>We first focus on <span class="image"><img src="feed-forward-neural-network/stem-7e6d86e31bebf5a6edc4a80a7c9207fa.png" alt="stem 7e6d86e31bebf5a6edc4a80a7c9207fa" width="36" height="15"></span> only, how do we calculate it ? We&#8217;ll start from the right, using the formula from the previous section. For now we will keep <span class="image"><img src="feed-forward-neural-network/stem-75bc06fd7c00a77b4071eedee1544865.png" alt="stem 75bc06fd7c00a77b4071eedee1544865" width="153" height="18"></span> and see what happens :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-729eba1b3851a9097fc3846b70bdd7db.png" alt="$$out_1 = activation(\sum_{i = 0}^{n\_in}(in_i \cdot weight_i)) = \sum_{i = 0}^{n\_in}(in_i \cdot weight_i)$$" width="492" height="56">
</div>
</div>
<div class="paragraph">
<p>Since we have more than one column and more than one unit per layer, we will refer to the weights by using the indexes <span class="image"><img src="feed-forward-neural-network/stem-a74b38a63de9219067741e6111cc1674.png" alt="stem a74b38a63de9219067741e6111cc1674" width="422" height="21"></span>.</p>
</div>
<div class="ulist">
<ul>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-37a0726b5aae657246c15b5f16daaa74.png" alt="stem 37a0726b5aae657246c15b5f16daaa74" width="114" height="17"></span> will be 0 for the weights associated to the arrows going from the input layer, 1 for the weights associated to the arrows going from the first hidden layer, and 2 for those going from the second hidden layer.</p>
</li>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-71258a33c2f8fe06bcdbc507b31f0a97.png" alt="stem 71258a33c2f8fe06bcdbc507b31f0a97" width="129" height="17"></span> Is the index of the unit within its own layer, at which the arrow points to. Note that for the output layer, we will start with the index <span class="image"><img src="feed-forward-neural-network/stem-034d0a6be0424bffe9a6e7ac9236c0f5.png" alt="stem 034d0a6be0424bffe9a6e7ac9236c0f5" width="8" height="12"></span> instead of <span class="image"><img src="feed-forward-neural-network/stem-29632a9bf827ce0200454dd32fc3be82.png" alt="stem 29632a9bf827ce0200454dd32fc3be82" width="11" height="14"></span>.</p>
</li>
<li>
<p><span class="image"><img src="feed-forward-neural-network/stem-7b216bd67232f0fce6e2349fbfc8136a.png" alt="stem 7b216bd67232f0fce6e2349fbfc8136a" width="161" height="17"></span> Is the index of the unit within its own layer, from which the arrow starts. The bias is index 0, and then it goes from top to bottom.</p>
</li>
</ul>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-23269562c1829092aad27ea389dc0ab2.png" alt="$$out_1 = \sum_{j = 0}^{5}(h_{2,j} \cdot weight_{2,1,j})$$" width="266" height="57">
</div>
</div>
<div class="paragraph">
<p>We can simplify this expression mathematically if instead of considering each unit <span class="image"><img src="feed-forward-neural-network/stem-32f841b46152dc609a3f3dceaa116cf5.png" alt="stem 32f841b46152dc609a3f3dceaa116cf5" width="35" height="20"></span> and <span class="image"><img src="feed-forward-neural-network/stem-4c0f33b2247ecdf6bb6e75f385f394e4.png" alt="stem 4c0f33b2247ecdf6bb6e75f385f394e4" width="105" height="20"></span>, we consider the vectors :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-2ccf3e623d2510d3b091a94ce553ffc3.png" alt="$$\mathbf{h}_2 = (h_{2,0}, h_{2,1}, h_{2,2}, h_{2,3}, h_{2,4}, h_{2,5})]$$" width="333" height="20">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-cba3720fd4553e926e35dccbc0b2dfd8.png" alt="$$\mathbf{weight}_{2,1} = (weight_{2,1,0}, weight_{2,1,1}, weight_{2,1,2}, weight_{2,1,3}, weight_{2,1,4}, weight_{2,1,5})]$$" width="825" height="20">
</div>
</div>
<div class="paragraph">
<p>Note the when we are talking about a vector of weights, we will use the indexes <span class="image"><img src="feed-forward-neural-network/stem-0bcb97786c94dc992cb847b899fb4117.png" alt="stem 0bcb97786c94dc992cb847b899fb4117" width="263" height="21"></span>.</p>
</div>
<div class="paragraph">
<p>We can rewrite the expression as :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-4a057954d407fc46a9d5c37c3c604a18.png" alt="$$out_1 = \mathbf{h}_2 \cdot \mathbf{weight}_{2,1}^T$$" width="194" height="23">
</div>
</div>
<div class="paragraph">
<p>What is this <span class="image"><img src="feed-forward-neural-network/stem-0103700fa3db36d24bf7a94ad80ae6a5.png" alt="stem 0103700fa3db36d24bf7a94ad80ae6a5" width="42" height="9"></span> ? It is because we want the transpose of the vector <span class="image"><img src="feed-forward-neural-network/stem-324b19458843af29c0bce64b0c4e34a8.png" alt="stem 324b19458843af29c0bce64b0c4e34a8" width="98" height="18"></span>. The transpose of a row vector makes it a column vector. This will allow us to use the matrix multiplication rule in order to calculate <span class="image"><img src="feed-forward-neural-network/stem-7e6d86e31bebf5a6edc4a80a7c9207fa.png" alt="stem 7e6d86e31bebf5a6edc4a80a7c9207fa" width="36" height="15"></span> :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-de160b5e91a6107c5df83b12c2c3258d.png" alt="$$out_1 = (h_{2,0}, h_{2,1}, h_{2,2}, h_{2,3}, h_{2,4}, h_{2,5}) \cdot
\begin{pmatrix}
weight_{2,1,0} \\
weight_{2,1,1} \\
weight_{2,1,2} \\
weight_{2,1,3} \\
weight_{2,1,4} \\
weight_{2,1,5}
\end{pmatrix}$$" width="336" height="105">
</div>
</div>
<div class="paragraph">
<p>Which is indeed equivalent to the previous formula we had :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-09d2c14ada84e0ade7f33aff774604a9.png" alt="$$out_1 = h_{2,0} \cdot weight_{2,1,0} + h_{2,1} \cdot weight_{2,1,1} + h_{2,2} \cdot weight_{2,1,2} + h_{2,3} \cdot weight_{2,1,3} + h_{2,4} \cdot weight_{2,1,4} + h_{2,5} \cdot weight_{2,1,5}$$" width="1098" height="18">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-0afc9d35789ad06f8e7c9297f7ccfb0f.png" alt="$$out_1 = \sum_{j = 0}^{5}(h_{2,j} \cdot weight_{2,j})$$" width="249" height="57">
</div>
</div>
<div class="paragraph">
<p>We can easily calculate <span class="image"><img src="feed-forward-neural-network/stem-c74ad31ad38caa9d69de7c80bae14ba0.png" alt="stem c74ad31ad38caa9d69de7c80bae14ba0" width="98" height="23"></span> because <span class="image"><img src="feed-forward-neural-network/stem-324b19458843af29c0bce64b0c4e34a8.png" alt="stem 324b19458843af29c0bce64b0c4e34a8" width="98" height="18"></span> is made of the neural network&#8217;s parameters and we know them by definition when we use the neural network. If we want to make this expression useful, we need to calculate <span class="image"><img src="feed-forward-neural-network/stem-3844330362935e352ae1a9db9d4ac09a.png" alt="stem 3844330362935e352ae1a9db9d4ac09a" width="21" height="17"></span>. It is quite easy to do so :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-c886c4b0fe691f6d71990127889dca5e.png" alt="$$\mathbf{h}_2 = (h_{2,0}, h_{2,1}, h_{2,2}, h_{2,3}, h_{2,4}, h_{2,5})$$" width="329" height="20">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-36f722f44e184677c046a9ac2af7b70d.png" alt="$$h_{2,0} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,0}^T$$" width="192" height="23">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-85b9d13073d76cea15514068ddab7f44.png" alt="$$h_{2,1} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,1}^T$$" width="189" height="23">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-5d5e6bc7d037736341d49004da4ee3d3.png" alt="$$h_{2,2} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,2}^T$$" width="192" height="23">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-6fe0542259d95392662408eb4492494b.png" alt="$$h_{2,3} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,3}^T$$" width="192" height="23">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-ada417f87fedb2ece2579b634ed9d923.png" alt="$$h_{2,4} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,4}^T$$" width="194" height="23">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-7d9bd3b8ba315210aa5753fe9d803250.png" alt="$$h_{2,5} = \mathbf{h}_1 \cdot \mathbf{weight}_{1,5}^T$$" width="192" height="23">
</div>
</div>
<div class="paragraph">
<p>The vector <span class="image"><img src="feed-forward-neural-network/stem-e8d187eb34b1bc2c34f809688aac0acb.png" alt="stem e8d187eb34b1bc2c34f809688aac0acb" width="95" height="18"></span> that we use is different for each <span class="image"><img src="feed-forward-neural-network/stem-b05862afd9b8c924f6ba30e0fce94c88.png" alt="stem b05862afd9b8c924f6ba30e0fce94c88" width="35" height="18"></span> because the arrows going to each unit are all different.</p>
</div>
<div class="paragraph">
<p>Once again we can mathematically simplify this expression if instead of considering each vectors separately, we consider the matrix :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-c8efcad3059571540f4f636443f84970.png" alt="$$Weight_1 =
\begin{pmatrix}
\mathbf{weight}_{1,0} \\
\mathbf{weight}_{1,1} \\
\mathbf{weight}_{1,2} \\
\mathbf{weight}_{1,3} \\
\mathbf{weight}_{1,4} \\
\mathbf{weight}_{1,5}
\end{pmatrix} =
\begin{pmatrix}
weight_{1,0,0}, weight_{1,0,1}, weight_{1,0,2}, weight_{1,0,3}, weight_{1,0,4}, weight_{1,0,5} \\
weight_{1,0,1}, weight_{1,1,1}, weight_{1,1,2}, weight_{1,1,3}, weight_{1,1,4}, weight_{1,1,5} \\
weight_{1,0,2}, weight_{1,2,1}, weight_{1,2,2}, weight_{1,2,3}, weight_{1,2,4}, weight_{1,2,5} \\
weight_{1,0,3}, weight_{1,3,1}, weight_{1,3,2}, weight_{1,3,3}, weight_{1,3,4}, weight_{1,3,5} \\
weight_{1,0,4}, weight_{1,4,1}, weight_{1,4,2}, weight_{1,4,3}, weight_{1,4,4}, weight_{1,4,5} \\
weight_{1,0,5}, weight_{1,5,1}, weight_{1,5,2}, weight_{1,5,3}, weight_{1,5,4}, weight_{1,5,5}
\end{pmatrix}$$" width="663" height="105">
</div>
</div>
<div class="paragraph">
<p>We can then rewrite the expression as :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-8bb6aa4b51e1edd0d96acda7c3665cf0.png" alt="$$\mathbf{h}_2  = \mathbf{h}_1 \cdot Weight_1^T$$" width="158" height="21">
</div>
</div>
<div class="paragraph">
<p>Note that we use the transpose for the same reason as before. This is how to transpose a matrix :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-4a9561ceea196044757f649d3add32c5.png" alt="$$\begin{pmatrix}
\color{red} {11, 12, 13, 14, 15} \\
\color{green} {21, 22, 23, 24, 25} \\
\color{blue} {31, 32, 33, 34, 35}
\end{pmatrix}^T
=
\begin{pmatrix}
\color{red} {11}, \color{green} {21}, \color{blue} {31} \\
\color{red} {12}, \color{green} {22}, \color{blue} {32} \\
\color{red} {13}, \color{green} {23}, \color{blue} {33} \\
\color{red} {14}, \color{green} {24}, \color{blue} {34} \\
\color{red} {15}, \color{green} {25}, \color{blue} {35}
\end{pmatrix}$$" width="242" height="76">
</div>
</div>
<div class="paragraph">
<p>Let&#8217;s apply the rules of matrix multiplication to be sure that we really get the same result. We start by the transpose :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-87b7a846e576d6df21ba810ed6b006cb.png" alt="$$\mathbf{h}_2 = (h_{1,0}, h_{1,1}, h_{1,2}, h_{1,3}, h_{1,4}, h_{1,5}) \cdot
\begin{pmatrix}
weight_{1,0,0}, weight_{1,0,1}, weight_{1,0,2}, weight_{1,0,3}, weight_{1,0,4}, weight_{1,0,5} \\
weight_{1,0,1}, weight_{1,1,1}, weight_{1,1,2}, weight_{1,1,3}, weight_{1,1,4}, weight_{1,1,5} \\
weight_{1,0,2}, weight_{1,2,1}, weight_{1,2,2}, weight_{1,2,3}, weight_{1,2,4}, weight_{1,2,5} \\
weight_{1,0,3}, weight_{1,3,1}, weight_{1,3,2}, weight_{1,3,3}, weight_{1,3,4}, weight_{1,3,5} \\
weight_{1,0,4}, weight_{1,4,1}, weight_{1,4,2}, weight_{1,4,3}, weight_{1,4,4}, weight_{1,4,5} \\
weight_{1,0,5}, weight_{1,5,1}, weight_{1,5,2}, weight_{1,5,3}, weight_{1,5,4}, weight_{1,5,5}
\end{pmatrix}^T$$" width="720" height="108">
</div>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-f81271fc6ecf80b1fa15f59e7316f7eb.png" alt="$$\mathbf{h}_2 = (h_{1,0}, h_{1,1}, h_{1,2}, h_{1,3}, h_{1,4}, h_{1,5}) \cdot
\begin{pmatrix}
weight_{1,0,0}, weight_{1,1,0}, weight_{1,2,0}, weight_{1,3,0}, weight_{1,4,0}, weight_{1,5,0} \\
weight_{1,0,1}, weight_{1,1,1}, weight_{1,2,1}, weight_{1,3,1}, weight_{1,4,1}, weight_{1,5,1} \\
weight_{1,0,2}, weight_{1,1,2}, weight_{1,2,2}, weight_{1,3,2}, weight_{1,4,2}, weight_{1,5,2} \\
weight_{1,0,3}, weight_{1,1,3}, weight_{1,2,3}, weight_{1,3,3}, weight_{1,4,3}, weight_{1,5,3} \\
weight_{1,0,4}, weight_{1,1,4}, weight_{1,2,4}, weight_{1,3,4}, weight_{1,4,4}, weight_{1,5,4} \\
weight_{1,0,5}, weight_{1,1,5}, weight_{1,2,5}, weight_{1,3,5}, weight_{1,4,5}, weight_{1,5,5}
\end{pmatrix}$$" width="713" height="105">
</div>
</div>
<div class="paragraph">
<p>The following expression is actually a vector of the form <span class="image"><img src="feed-forward-neural-network/stem-ed4809965d58597ce346829896f55b0f.png" alt="stem ed4809965d58597ce346829896f55b0f" width="138" height="18"></span>. I write it as rows for it is easier to read.</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-ec8b5a9f8d7b95bd071cb14bed036f45.png" alt="$$\mathbf{h}_2 =
\begin{pmatrix}
h_{1,0} \cdot weight_{1,0,0} + h_{1,1} \cdot weight_{1,0,1} + h_{1,2} \cdot weight_{1,0,2} + h_{1,3} \cdot weight_{1,0,3} + h_{1,4} \cdot weight_{1,0,4} + h_{1,5} \cdot weight_{1,0,5} \\
h_{1,0} \cdot weight_{1,1,0} + h_{1,1} \cdot weight_{1,1,1} + h_{1,2} \cdot weight_{1,1,2} + h_{1,3} \cdot weight_{1,1,3} + h_{1,4} \cdot weight_{1,1,4} + h_{1,5} \cdot weight_{1,1,5} \\
h_{1,0} \cdot weight_{1,2,0} + h_{1,1} \cdot weight_{1,2,1} + h_{1,2} \cdot weight_{1,2,2} + h_{1,3} \cdot weight_{1,2,3} + h_{1,4} \cdot weight_{1,2,4} + h_{1,5} \cdot weight_{1,2,5} \\
h_{1,0} \cdot weight_{1,3,0} + h_{1,1} \cdot weight_{1,3,1} + h_{1,2} \cdot weight_{1,3,2} + h_{1,3} \cdot weight_{1,3,3} + h_{1,4} \cdot weight_{1,3,4} + h_{1,5} \cdot weight_{1,3,5} \\
h_{1,0} \cdot weight_{1,4,0} + h_{1,1} \cdot weight_{1,4,1} + h_{1,2} \cdot weight_{1,4,2} + h_{1,3} \cdot weight_{1,4,3} + h_{1,4} \cdot weight_{1,4,4} + h_{1,5} \cdot weight_{1,4,5} \\
h_{1,0} \cdot weight_{1,5,0} + h_{1,1} \cdot weight_{1,5,1} + h_{1,2} \cdot weight_{1,5,2} + h_{1,3} \cdot weight_{1,5,3} + h_{1,4} \cdot weight_{1,5,4} + h_{1,5} \cdot weight_{1,5,5}
\end{pmatrix}$$" width="748" height="105">
</div>
</div>
<div class="paragraph">
<p>If we take the same expression as when we calculated <span class="image"><img src="feed-forward-neural-network/stem-7e6d86e31bebf5a6edc4a80a7c9207fa.png" alt="stem 7e6d86e31bebf5a6edc4a80a7c9207fa" width="36" height="15"></span> we obtain :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-6d5bf2fdb0a073f7029cebdc699fd0c8.png" alt="$$\mathbf{h}_2 = (\mathbf{h}_1 \cdot \mathbf{weight}_{1,0}^T, \mathbf{h}_1 \cdot \mathbf{weight}_{1,1}^T, \mathbf{h}_1 \cdot \mathbf{weight}_{1,2}^T, \mathbf{h}_1 \cdot \mathbf{weight}_{1,3}^T, \mathbf{h}_1 \cdot \mathbf{weight}_{1,4}^T, \mathbf{h}_1 \cdot \mathbf{weight}_{1,5}^T)$$" width="912" height="23">
</div>
</div>
<div class="paragraph">
<p>Which is indeed equal to the expression we had earlier !</p>
</div>
<div class="paragraph">
<p>Now the last step, we want to calculate <span class="image"><img src="feed-forward-neural-network/stem-5a95dbebd5e79e850a576db54f501ab8.png" alt="stem 5a95dbebd5e79e850a576db54f501ab8" width="17" height="17"></span> in function of the inputs. This time it&#8217;s simple, we just have to use the same technique as we did to calculate <span class="image"><img src="feed-forward-neural-network/stem-0f7cea0b89929faf20eda59174bc247f.png" alt="stem 0f7cea0b89929faf20eda59174bc247f" width="18" height="17"></span> :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-cdb85ab5bfc80c80223304a781be0048.png" alt="$$\mathbf{h}_1  = \mathbf{in} \cdot Weight_0^T$$" width="158" height="21">
</div>
</div>
<div class="paragraph">
<p>To summarize everything that we did :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-c2be82a8c1e293627fc60401a342eb6f.png" alt="$$out_1 = \mathbf{h}_2 \cdot \mathbf{weight}_{2,1}^T \
out_1 = (\mathbf{h}_1 \cdot Weight_1^T) \cdot \mathbf{weight}_{2,1}^T \
out_1 = ((\mathbf{in} \cdot Weight_0^T) \cdot Weight_1^T) \cdot \mathbf{weight}_{2,1}^T$$" width="602" height="15">
</div>
</div>
<div class="paragraph">
<p>There is still one thing : we have more than one output, so instead of just calculating <span class="image"><img src="feed-forward-neural-network/stem-7e6d86e31bebf5a6edc4a80a7c9207fa.png" alt="stem 7e6d86e31bebf5a6edc4a80a7c9207fa" width="36" height="15"></span>, we can calculate all the <span class="image"><img src="feed-forward-neural-network/stem-402aa84f37e5cf42cbbe2539eaefbf9a.png" alt="stem 402aa84f37e5cf42cbbe2539eaefbf9a" width="35" height="15"></span> of the vector <span class="image"><img src="feed-forward-neural-network/stem-12cb177289fd42a6730c1523fbf3c569.png" alt="stem 12cb177289fd42a6730c1523fbf3c569" width="36" height="12"></span> at the same time :</p>
</div>
<div class="imageblock text-center">
<div class="content">
<img src="feed-forward-neural-network/stem-a35c1e6bc4e62f566a973a54ee9b1836.png" alt="$$\mathbf{out} = ((\mathbf{in} \cdot Weight_0^T) \cdot Weight_1^T) \cdot Weight_{2}^T$$" width="386" height="21">
</div>
</div>
<div class="paragraph">
<p>Do you get it ? Right, we only have to consider the matrix of weights <span class="image"><img src="feed-forward-neural-network/stem-b287dcfcd1629ede30de8018c9ba9959.png" alt="stem b287dcfcd1629ede30de8018c9ba9959" width="78" height="21"></span> instead of the vector <span class="image"><img src="feed-forward-neural-network/stem-c74ad31ad38caa9d69de7c80bae14ba0.png" alt="stem c74ad31ad38caa9d69de7c80bae14ba0" width="98" height="23"></span> !</p>
</div>
<div class="paragraph">
<p><strong>This is the equation of the neural network.</strong> By filling the input <span class="image"><img src="feed-forward-neural-network/stem-aeef64a84c112007afce89a8d750276f.png" alt="stem aeef64a84c112007afce89a8d750276f" width="21" height="14"></span> into it, you can calculate the prediction <span class="image"><img src="feed-forward-neural-network/stem-12cb177289fd42a6730c1523fbf3c569.png" alt="stem 12cb177289fd42a6730c1523fbf3c569" width="36" height="12"></span>.</p>
</div>
<div class="paragraph">
<p>Have you ever heard that deep learning is mainly about matrix multiplications ? Here&#8217;s why ! Not only is this representation easier to read but there exists algorithms for matrix multiplication that are more efficient than just doing all the additions and multiplications straight away.</p>
</div>
</div>
</div>
<div class="sect1">
<h2 id="_conclusion">Conclusion</h2>
<div class="sectionbody">
<div class="paragraph">
<p>We saw that the most basic form of neural network is actually a simple linear equation. From there, we escalated to a more complex neural network and introduced matrices along the way as a mean to not only simplify the equations but also to reduce the problem to one that is well known in computer sciences : <strong>matrix multiplications</strong>.</p>
</div>
<div class="paragraph">
<p>You now know how to use a neural network to make a prediction by calculated its equation.</p>
</div>
<div class="paragraph">
<p>There is still one more piece : to make the model of the neural network accurate, its parameters, the weights, have to be set accordingly. This is what be call the learning phase. I will show in the next article how we can do that efficiently by an algorithm know as backpropagation.</p>
</div>
</div>
</div>